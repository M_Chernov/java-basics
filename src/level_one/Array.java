package level_one;

import java.util.Arrays;

public class Array {
/*
firstLast6
Given an array of ints, return true if 6 appears as either the first or last element in the array.
The array will be length 1 or more.

commonEnd
Given 2 arrays of ints, a and b, return true if they have the same first element or they have the same last element.
Both arrays will be length 1 or more.

sum3
Given an array of ints length 3, return the sum of all the elements.
*/

    public boolean firstLast6(int[] nums) {
        int a = nums.length;
        if (nums[0] == 6 || nums[a - 1] == 6) {
            return true;
        }
        else return false;
    }

    public boolean commonEnd(int[] a, int[] b) {
        if (a[0] == b[0]) {
            return true;
        }
        else if (a[a.length - 1] == b[b.length - 1]) {
                return true;
        }
        else return false;
    }

    public int sum3(int[] nums) {
        return Arrays.stream(nums).sum();
    }
}
