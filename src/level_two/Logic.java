package level_two;

public class Logic {
/*
loneSum
Given 3 int values, a b c, return their sum.
However, if one of the values is the same as another of the values, it does not count towards the sum.

luckySum
Given 3 int values, a b c, return their sum. However,
if one of the values is 13 then it does not count towards the sum and values to its right do not count.
So for example, if b is 13, then both b and c do not count.
*/

    public int loneSum(int a, int b, int c) {
        if (a == b && a == c)
            return 0;
        else if (a == b && b != c)
            return c;
        else if (a == c && c != b)
            return b;
        else if (b == c && c != a)
            return a;
        else return a + b + c;
    }

    public int luckySum(int a, int b, int c) {
        if (a == 13)
            return 0;
        else if (b == 13)
            return a;
        else if (c == 13)
            return  a + b;
        else return a + b + c;
    }
}
